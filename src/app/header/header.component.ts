import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../auth/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private _router: Router, private _us: UserService) { }

  public isadmin:boolean;

  ngOnInit(): void {
    this.isadmin = this._us.isadmin;
  }

  logout() {
    this._router.navigate([""]);
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatToolbarModule, MatButtonModule, MatInputModule, MatCardModule, MatGridListModule, MatProgressSpinnerModule, MatPaginatorModule } from '@angular/material';
import { MatVideoModule } from 'mat-video';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CoursesComponent } from './courses/courses.component';
import { CourseShowComponent } from './courses/course-show/course-show.component';
import { CourseCreateComponent } from './courses/course-create/course-create.component';
import { FooterComponent } from './footer/footer.component';
import { SafePipe } from './courses/safe.pipe';
import { CertificateComponent } from './courses/certificate/certificate.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    CoursesComponent,
    CourseShowComponent,
    CourseCreateComponent,
    FooterComponent,
    SafePipe,
    CertificateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatVideoModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatPaginatorModule
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

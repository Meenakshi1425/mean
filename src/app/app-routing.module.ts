import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseShowComponent } from './courses/course-show/course-show.component';
import { CourseCreateComponent } from './courses/course-create/course-create.component';
import { CertificateComponent } from './courses/certificate/certificate.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: "courses",
    component: CoursesComponent
  },
  {
    path: "course/show/:id",
    component: CourseShowComponent
  },
  {
    path: "course/new",
    component: CourseCreateComponent
  },
  {
    path: "certificate",
    component: CertificateComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

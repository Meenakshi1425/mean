import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _us:UserService, private _router: Router) { }

  public user:User = { username: "Dummy", email: "", password: "" };

  ngOnInit(): void {
  }

  login(form) {
    this._us.login(form.value).subscribe(
      res => {

        this._router.navigate(["/about"]);
      }
    )
  }

}

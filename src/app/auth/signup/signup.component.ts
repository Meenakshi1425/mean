import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private _us:UserService, private _router: Router) { }

  public user:User = { username: "", email: "", password: "" };

  ngOnInit(): void {
  }

  signup(form) {
    this._us.signup(form.value).subscribe(
      res => {
        this._router.navigate(["/about"]);
      }
    )
  }

}

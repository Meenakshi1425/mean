import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) { }

  public user:User = { username: "", email: "", password: "" };

  private _dburl = "http://localhost:3000/api/user";

  public isadmin:boolean = true;

  public login(user:User):Observable<User> {
    return this._http.post<User>(this._dburl + "/login", user);
  }

  public signup(user:User):Observable<User> {
    return this._http.post<User>(this._dburl + "/signup", user);
  }
}

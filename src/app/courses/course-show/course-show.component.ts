import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CourseService } from '../course.service';
import { SafePipe } from '../safe.pipe';

@Component({
  selector: 'app-course-show',
  templateUrl: './course-show.component.html',
  styleUrls: ['./course-show.component.css']
})
export class CourseShowComponent implements OnInit {

  public id:string = "";
  public course:any = { name: "", description: "", uri: "" };

  constructor(private _route: ActivatedRoute, private _cs: CourseService, private _router:Router) {
    this.id = this._route.snapshot.params.id;
  }

  certificate() {
    this._router.navigate(["/certificate"]);
  }

  ngOnInit(): void {
    this._cs.get_course(this.id).subscribe(
      res => {
        this.course = res;
      }
    )
  }

}

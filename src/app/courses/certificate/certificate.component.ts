import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css']
})
export class CertificateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  img:any = "../../home/cover.jpg";

  certificate() {
    var element = document.getElementById('certificate');
    html2canvas(element, { allowTaint: false, useCORS: true }).then((canvas) => {
      var imgdata = canvas.toDataURL('image/png');
      console.log("canvas", canvas);
      var doc = new jspdf();
      var imgHeight = canvas.height * 340 / canvas.width;
      doc.addImage(imgdata, 0, 0, 208, imgHeight);
      doc.save('certificate.pdf');
    })
  }
}

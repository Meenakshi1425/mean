import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Course } from './course';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private _http: HttpClient) { }

  public url = "http://localhost:3000/api/course"

  public upload_course(course):Observable<Course> {
    console.log("course", course);
    return this._http.post<Course>(this.url + "/upload", course);
  }

  public get_all_courses(pagesize:number, page:number):Observable<Course[]> {
    const query = `?pagesize=${pagesize}&page=${page}`;
    return this._http.get<Course[]>(this.url + "/videos" + query);
  }

  public get_course(id:string):Observable<Course> {
    return this._http.get<Course>(this.url + "/video/" + id);
  }
}

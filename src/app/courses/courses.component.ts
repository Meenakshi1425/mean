import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CourseService } from './course.service';
import { SafePipe } from './safe.pipe';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  constructor(private _router: Router, private _cs: CourseService) { }

  courses:any[] = [];

  totalpage = 10;
  page = 1;
  pagesize = 5;

  ngOnInit(): void {
    this._cs.get_all_courses(this.pagesize, this.page).subscribe(
      res => {
        console.log("res", res);
        this.courses = res;
      }
    )
  }

  pagechange(pageevent: PageEvent) {
    console.log("pageevent", pageevent);
    this._cs.get_all_courses(this.pagesize, pageevent.pageIndex + 1).subscribe(
      res => {
        console.log("res", res);
        this.courses = res;
      }
    )
  }

  show(id:string) {
    this._router.navigate(["/course/show/", id])
  }

}

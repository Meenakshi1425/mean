const express = require('express');
const router = express.Router();
const Course = require('../models/course');
const path = require('path');
const Vimeo = require('vimeo').Vimeo;
const multer = require('multer');
const client = new Vimeo("6a54366a88cb8efa68fa83f5377577e930fb8344", "8YbbhDOHuJnYqiQH+EjaclxDrKJ/o9R8c3hoDLQtVDE0G4kIRdYIxTtJiCw88rVXFiQCVLEJ+oWjRVNJx2gW8z8/LuvS3Gx5+kbE4WDCBJifHBWpFxQSRW6/bJpb/6DS", "c4400fabd3143a084b9024d3722f33e4");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'backend/videos');
  },
  filename: (req, file, cb) => {
    cb(null, 'video.mp4')
  }
});

var upload = multer({storage: storage});

router.post("/upload", upload.single('uri'), (req, res, next) => {
  var file_name = path.join(__dirname, '../videos/video.mp4');
  client.upload(
    file_name,
    {
      'name': req.body.name,
      'description': req.body.description
    },
    function (uri) {
      const course = new Course({
        name: req.body.name,
        description: req.body.description,
        uri: `https://player.vimeo.com/video/${uri.split('/').pop()}`
      })

      course.save().then((result) => {
        console.log("result", result)
        res.status(200).send(result);
      })
      console.log('Your video URI is: ' + uri);
    },
    function (bytes_uploaded, bytes_total) {
      var percentage = (bytes_uploaded / bytes_total * 100).toFixed(2)
      console.log(bytes_uploaded, bytes_total, percentage + '%')
    },
    function (error) {
      res.send({ error: error });
      console.log('Failed because: ' + error);
    }
  )
});

router.get("/videos", (req, res, next) => {
  const pagesize = +req.query.pagesize;
  const page = +req.query.page;
  const courseQuery = Course.find();
  if(pagesize && page) {
    courseQuery.skip(pagesize * (page - 1)).limit(pagesize);
  }
  courseQuery.then((result) => { res.status(200).send(result) }).catch((err) => { res.status(400).send(err) })
})

router.get("/video/:id", (req, res, next) => {
  Course.findOne({_id: req.params.id }).then((result) => { res.status(200).send(result) })
})

module.exports = router;



const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');
const app = express();

app.use(bodyparser.json());
app.use(cors());

mongoose.connect('mongodb://localhost:27017/training').then(
  () => { console.log("DB connected successfully.") }
).catch(
  (err) => { console.log("There was a problem with DB connection.") }
);

app.use("/api/user", userRoutes);
app.use("/api/course", courseRoutes);

module.exports = app;
